import { Image, ImageBackground, StyleSheet, Text, View, } from 'react-native'
import React, { useEffect } from 'react'
import { Background, logo } from '../../assets'


const Splash = ({navigation}) => {
  
  useEffect ( () =>{
    setTimeout(() => {
    navigation.replace('MainApp');
  },4000);
}, [navigation]);

  return ( 
    <ImageBackground source={ Background} style={styles.Background}>
    <Image source={logo} style={styles.logo}/>
    </ImageBackground>
  )
}

export default Splash

const styles = StyleSheet.create({
  Background:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 222,
    height: 105

  }
})