import { StyleSheet, Text, View, StatusBar, TouchableOpacity } from 'react-native'
import React from 'react'

const Home = () => {
  return (
    <View style={{flex:1, backgroundColor:'#3b0991'}}>
    <StatusBar backgroundColor="#3b0991" />
      <Text style={{ color:'#ffff', fontSize:20, textAlign:'center'}}>Beranda</Text>
      <TouchableOpacity style={{borderRadius:30, backgroundColor:'#ffff', marginHorizontal:20}}>
      <Text style={{ color:'#9c0909', fontSize:20, textAlign:'center'}}>React Native</Text>
      </TouchableOpacity>
    </View>
  )
}

export default Home

const styles = StyleSheet.create({})